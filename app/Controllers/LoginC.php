<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\LoginM;


class LoginC extends BaseController
{

	public $session=null;
	public function __construct(){
		$this->session = \Config\Services::session();
	}
	public function index(){
		echo view('template/header');
		echo view('LoginView');
		echo view('template/footer');
			
		
	}



	public function iniciar(){

		$model = new LoginM;
		$request= \Config\Services::request();

			$datos['usu'] = $request->getPost('usu'); 
			$datos['cla'] = $request->getPost('pass');

			$si = $model->validar($datos);

			if ($si) {
				$data =array('usuario' => $si->usuario, 'contraseña' => $si->contraseña, 'logeado' => 'true');
				$this->session->set($data);
				echo view('welcome_message');
			}else{
				echo 'Credenciales erroneas';
			}
			
			
				
		
	}

	public function cerrar(){
		$data = array('logeado' => 'false');
		$this->session->set($data);
		$this->session->destroy();

		echo view('template/header');
		echo view('LoginView');
		echo view('template/footer');
	}
}





